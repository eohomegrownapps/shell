# RACTF Shell

"Shell" is just a fun name for the SPA frontend I came up with when I realised
I needed to pick a name when making the project. This is probably going to take
ages to get properly working, so it's probably best not to touch stuff here
until the first builds are working.

## // TODO:

- Get a proper CI running with azure
- Get storybook setup for testing of react components
- Get feature partity with the MPA

## Running the SPA

I'm sure you're a smart cookie and can figure it out yourself, but if you're not, use `npm start` to start the site, and `npm run build` to compile a production build.
