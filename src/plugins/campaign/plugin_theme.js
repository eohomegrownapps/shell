export const link_size = "3%";
export const node_size = "25%";
export const node_inner = "9rem";
export const link_part = "10%";
export const node_margin = "5%";
export const icon_size = "100%";
